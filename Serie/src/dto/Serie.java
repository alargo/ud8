package dto;

public class Serie {
	String titulo;
	int numeroTemporada;
	boolean entregado;
	String genero;
	String creador;
	
	final boolean entregadoFinal = false;
	final int temporadaFinal = 3;
	
	//Autor: Alejandro Largo
	
	//Se  crean las variables fijas que se le pasa en algunos contructores para que esos datos no se puedan a�adir en el main por el usuairo
	
	public Serie(String titulo, int numeroTemporada, String genero, String creador) {
		super();
		this.titulo = titulo;
		this.numeroTemporada = numeroTemporada;
		this.genero = genero;
		this.creador = creador;
		this.entregado = entregadoFinal;
	}

	public Serie() {
		super();
		this.titulo = "";
		this.numeroTemporada = temporadaFinal;
		this.entregado = entregadoFinal;
		this.genero = "";
		this.creador = "";
	}

	public Serie(String titulo, String creador) {
		super();
		this.titulo = titulo;
		this.numeroTemporada = temporadaFinal;
		this.entregado = entregadoFinal;
		this.genero = "";
		this.creador = creador;
	}

	@Override
	public String toString() {
		return "Serie [titulo=" + titulo + ", numeroTemporada=" + numeroTemporada + ", entregado=" + entregado
				+ ", genero=" + genero + ", creador=" + creador + "]";
	}
	
	
	
	
	
	
}
