import dto.Serie;

public class SerieApp {

	public static void main(String[] args) {
		Serie s1 = new Serie();
		Serie s2 = new Serie("TWD","Alejandro");
		Serie s3 = new Serie("TLOU",2,"Suspenso","Alejandro Largo");
		
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);

	}

}
