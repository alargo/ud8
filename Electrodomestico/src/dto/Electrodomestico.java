package dto;

public class Electrodomestico {
	//Autor: Alejandro Largo
	
	//Se tiene las variables y las variables finales con el valor por defecto defindo 

	double precioBase;
	String color;
	char ConsumoEnergetico;
	double peso;
	
	final String colorDefecto = "Blanco";
	final char consumoDefecto =  'F';
	final double precioBaseDefecto = 100;
	final double pesoDefecto = 5;
	
	//En cada constructor se devuelve un atributo por defecto o uno que el usuario va a escoger
	
	public Electrodomestico() {
		super();
		this.precioBase = precioBaseDefecto;
		this.color = colorDefecto;
		this.ConsumoEnergetico = consumoDefecto;
		this.peso = pesoDefecto;
	}

	public Electrodomestico(double precioBase, double peso) {
		super();
		this.precioBase = precioBase;
		this.color = colorDefecto;
		ConsumoEnergetico = consumoDefecto;
		this.peso = peso;
	}

	public Electrodomestico(double precioBase, String color, char consumoEnergetico, double peso) {
		super();
		this.precioBase = precioBase;
		this.color = color;
		ConsumoEnergetico = consumoEnergetico;
		this.peso = peso;
	}

	//Se obtiene el color y con el switch se comprueba si el color esta o no 
	public String getColor() {
		return color;
	}
	
	
	public void obtenerColor (Electrodomestico color) {
		switch (color.getColor()) {
		case "NEGRO":
			
			break;
		case "BLANCO":
			System.out.println("Este color esta disponible");
			break;
		case "ROJO":
			System.out.println("Este color esta disponible");
			break;
			
		case "AZUL":
			System.out.println("Este color esta disponible");
			break;
			
		case "GRIS":
			System.out.println("Este color esta disponible");
			break;
			
		case "negro":
			System.out.println("Este color esta disponible");
			break;
			
		case "blanco":
			System.out.println("Este color esta disponible");
			break;
			
		case "rojo":
			System.out.println("Este color esta disponible");
			break;
			
		case "azul":
			System.out.println("Este color esta disponible");
			break;
			
		case "gris":
			System.out.println("Este color esta disponible");
			break;

		default:
			System.out.println("este color no esta disponible");
			break;
		}
	}

	@Override
	public String toString() {
		return "Electrodomestico [precioBase=" + precioBase + ", color=" + color + ", ConsumoEnergetico="
				+ ConsumoEnergetico + ", peso=" + peso + ", colorDefecto=" + colorDefecto + ", consumoDefecto="
				+ consumoDefecto + ", precioBaseDefecto=" + precioBaseDefecto + ", pesoDefecto=" + pesoDefecto + "]";
	}
	
	
	
	
	
	
	

}
