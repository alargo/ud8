import dto.Electrodomestico;

public class ElectrodomesticoApp {

	public static void main(String[] args) {
		Electrodomestico e1 = new Electrodomestico();
		Electrodomestico e2 = new Electrodomestico(200,6);
		Electrodomestico e3 = new Electrodomestico(200,"NARANJA",'A',9);
		

		System.out.println(e1);
		System.out.println(e2);
		System.out.println(e3);
	}

}
