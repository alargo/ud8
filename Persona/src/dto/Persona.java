package dto;

public class Persona {
	private String nombre;
	private int edad;
	private String dni;
	private char sexo;
	private double peso;
	private double altura;
	
	private final char SexoLetra = 'H';
	
	//Autor: Alejandro Largo
	
	//Se tienen las variables y los contructores donde van a devolver un dato fijo o uno que le pase el usuario 
	public Persona() {
		
		this.nombre = "";
		this.edad = 0;
		this.dni = "987654321K";
		this.sexo = SexoLetra;
		this.peso = 0;
		this.altura = 0;
	}

	public Persona(String nombre, int edad, char sexo) {
		
		this.nombre = nombre;
		this.edad = edad;
		this.dni = "";
		this.sexo = sexo;
		this.peso = 0;
		this.altura = 0;
	}

	public Persona(String nombre, int edad, String dni, char sexo, double peso, double altura) {

		this.nombre = nombre;
		this.edad = edad;
		this.dni = dni;
		this.sexo = sexo;
		this.peso = peso;
		this.altura = altura;
	}

	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + ", dni=" + dni + ", sexo=" + sexo + ", peso=" + peso
				+ ", altura=" + altura + "]";
	}
	

	
	
}


