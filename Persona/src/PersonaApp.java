import dto.Persona;

public class PersonaApp {

	public static void main(String[] args) {
		
		Persona p1 = new Persona();
		Persona p2 = new Persona("Alejandro",23,'H');
		Persona p3 = new Persona("Alejandro",23,"789654123J",'H',82.5,178);
		
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
		
	}

}
